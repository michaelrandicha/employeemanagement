import { EmployeeEffect } from './_reducers/employee/employee.effect';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { registerLocaleData } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import localeId from '@angular/common/locales/id';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginPageComponent } from './login-page/login-page.component';
import { E404Component } from './e404/e404.component';
import { authReducer } from './_reducers/auth/auth.reducer';
import { DashboardComponent } from './authenticated/dashboard/dashboard.component';
import { NavbarComponent } from './shared/component/navbar/navbar.component';
import { employeeReducer } from './_reducers/employee/employee.reducer';
import { AuthEffect } from './_reducers/auth/auth.effect';
import { DetailComponent } from './authenticated/detail/detail.component';
import { EditComponent } from './authenticated/edit/edit.component';
import { AddComponent } from './authenticated/add/add.component';

registerLocaleData(localeId)

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    E404Component,
    DashboardComponent,
    NavbarComponent,
    DetailComponent,
    EditComponent,
    AddComponent
  ],
  imports: [
    AppRoutingModule,
    AlertModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    EffectsModule.forRoot([AuthEffect, EmployeeEffect]),
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({auth: authReducer, employee: employeeReducer}),
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
