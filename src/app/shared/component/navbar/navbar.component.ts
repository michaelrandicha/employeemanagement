import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { logout } from 'src/app/_reducers/auth/auth.actions';
import { resetEmployee } from 'src/app/_reducers/employee/employee.actions';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(private store: Store<{ auth: boolean, employee: any }>, private router: Router) { }

  reset() {
    this.store.dispatch(resetEmployee())
  }

  logout() {
    this.store.dispatch(logout())
  }
}
