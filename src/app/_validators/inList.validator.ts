import { ValidatorFn, ValidationErrors, AbstractControl } from "@angular/forms";

export function inList( list: any[] ): ValidatorFn {

  return (control: AbstractControl): ValidationErrors | null => {

    let value = control.value;
   
    if (!list.find(data => data === value)) {
      return {
        inList: true
      };
    }
    return null;
  }
}