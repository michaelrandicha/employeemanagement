import { Action, createReducer, on } from '@ngrx/store';
import { addEmployee, editEmployee, deleteEmployee, saveConfig, deleteConfig, resetEmployee } from './employee.actions';
import DEFAULT_EMPLOYEE from '../../mock/employee.json';

export const initialState = localStorage.data ? JSON.parse(localStorage.data) : {employee:  DEFAULT_EMPLOYEE}
 
const _employeeReducer = createReducer(
  initialState,
  on(addEmployee, (state, {employee}) => {
    let newState = {...state, employee: [...state.employee, {id: state.employee.at(-1).id + 1, ...employee}]}
    localStorage.data = JSON.stringify(newState)
    return newState
  }),
  on(editEmployee, (state, {employee}) => {
    let newState = {...state, 
      employee: state.employee.map((data: any) => (data.id === employee.id) ? employee : data)
    }
    localStorage.data = JSON.stringify(newState)
    return newState
  }),
  on(deleteEmployee, (state, {id}) => {
    let newState = {...state, employee: state.employee.filter((value: any) => value.id !== id)}
    localStorage.data = JSON.stringify(newState)
    return newState
  }),
  on(resetEmployee, () => {
    localStorage.data = JSON.stringify({employee:  DEFAULT_EMPLOYEE})
    return {employee:  DEFAULT_EMPLOYEE}
  }),
  on(saveConfig, (state, {config}) => {
    let newState = {...state, config: config}
    localStorage.data = JSON.stringify(newState)
    return {...state, config: config}
  }),
  on(deleteConfig, (state) => {
    localStorage.data = JSON.stringify({employee: state.employee})
    return {employee: state.employee}
  }),
);
 
export function employeeReducer(state: any, action: Action) {
  return _employeeReducer(state, action);
}