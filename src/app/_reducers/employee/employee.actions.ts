import { createAction, props } from '@ngrx/store';

export const addEmployee = createAction('[Employee] Add Employee', props<{ employee: any }>());
export const editEmployee = createAction('[Employee] Edit Employee', props<{ employee: any }>());
export const deleteEmployee = createAction('[Employee] Delete Employee', props<{ id: number }>());
export const resetEmployee = createAction('[Employee] Reset Employee');

export const saveConfig = createAction('[Employee] Save Config', props<{ config: any }>());
export const deleteConfig = createAction('[Employee] Delete Config');