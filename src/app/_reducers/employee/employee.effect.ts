import { Injectable } from '@angular/core';
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { addEmployee, editEmployee, resetEmployee } from "./employee.actions";

@Injectable()
export class EmployeeEffect {
    resetEmployee$ = createEffect(() => this.actions$.pipe(
        ofType(resetEmployee),
        tap(() => window.location.reload())
    ), {dispatch: false})

    editEmployee$ = createEffect(() => this.actions$.pipe(
        ofType(editEmployee, addEmployee),
        tap(() => this.router.navigate(['/dashboard']))
    ), {dispatch: false})
    
    constructor(private actions$: Actions, private router: Router){}
}