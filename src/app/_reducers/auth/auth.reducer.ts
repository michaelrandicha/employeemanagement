import { Action, createReducer, on } from '@ngrx/store';
import { login, logout } from './auth.actions';

export const initialState = localStorage.isLoggedIn ? JSON.parse(localStorage.isLoggedIn) : false;
 
const _authReducer = createReducer(
  initialState,
  on(login, () => {
    localStorage.isLoggedIn = true
    return true
  }),
  on(logout, () => {
    localStorage.removeItem('isLoggedIn')
    return false
  }),
);
 
export function authReducer(state: any, action: Action) {
  return _authReducer(state, action);
}