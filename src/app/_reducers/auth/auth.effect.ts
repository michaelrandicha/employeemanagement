import { Injectable } from '@angular/core';
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { login, logout } from "./auth.actions";

@Injectable()
export class AuthEffect {
    login$ = createEffect(() => this.actions$.pipe(
        ofType(login),
        tap(() => this.router.navigate(['/dashboard']))
    ), {dispatch: false})

    logout$ = createEffect(() => this.actions$.pipe(
        ofType(logout),
        tap(() => this.router.navigate(['']))
    ), {dispatch: false})
    
    constructor(private actions$: Actions, private router: Router){}
}