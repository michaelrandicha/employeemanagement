import { AddComponent } from './authenticated/add/add.component';
import { EditComponent } from './authenticated/edit/edit.component';
import { DetailComponent } from './authenticated/detail/detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { E404Component } from './e404/e404.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardComponent } from './authenticated/dashboard/dashboard.component';
import { AuthGuard } from './_guard/auth.guard';

const routes: Routes = [
  { path: "", component: LoginPageComponent },
  { path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'dashboard/add', component: AddComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/detail/:id', component: DetailComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/edit/:id', component: EditComponent, canActivate: [AuthGuard] },
  { path: "404", component: E404Component },
  { path: "**", redirectTo: '/404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
