import { Location } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-e404',
  templateUrl: './e404.component.html',
  styleUrls: ['./e404.component.css']
})
export class E404Component {

  constructor(private location: Location) { }

  goBack() {
    this.location.back();
  }
}
