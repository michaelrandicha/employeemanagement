import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  employee: any;

  constructor(private location: Location, private store: Store<{ employee: any }>, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.store.select('employee').subscribe(value => {
      this.employee = value.employee.find((employee: any) => employee.id == Number(this.route.snapshot.paramMap.get('id')))
      if (!this.employee) this.router.navigate(['/dashboard'])
    })
  }

  goBack() {
    this.location.back();
  }
}
