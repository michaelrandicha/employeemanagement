import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { deleteEmployee, saveConfig } from '../../_reducers/employee/employee.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  employeeData: any
  selectedId: number;
  public displayedColumns: string[] = ['firstName', 'lastName', 'email', 'action'];
  public dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort) set matSort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  
  @ViewChild(MatPaginator) set matPaginator(paginator: MatPaginator) {
    this.dataSource.paginator = paginator
  }

  firstName: string 
  lastName: string
  email: string

  sortActive: any
  sortDirection: any

  pageIndex: any
  pageSize: any
  
  constructor(private modalService: NgbModal, private store: Store<{ employee: any }>, private router: Router) {}
  
  ngOnInit(): void {
    this.store.select('employee').subscribe(value => {
      this.employeeData = value.employee;
      this.dataSource.data = this.employeeData;

      this.firstName = value.config?.filter?.firstName
      this.lastName = value.config?.filter?.lastName
      this.email = value.config?.filter?.email

      this.filterData()

      this.sortActive = value.config?.sort?.active
      this.sortDirection = value.config?.sort?.direction
      
      this.pageIndex = value.config?.pagination?.index
      this.pageSize = value.config?.pagination?.size
      
      localStorage.data = JSON.stringify({ employee: this.employeeData })
    })
  }

  filterData() {
    this.dataSource.data = this.employeeData.filter((value: any) => 
      (this.firstName ? value.firstName.toLowerCase().startsWith(this.firstName.toLowerCase()) : true) &&
      (this.lastName ? value.lastName.toLowerCase().startsWith(this.lastName.toLowerCase()) : true) &&
      (this.email ? value.email.toLowerCase().startsWith(this.email.toLowerCase()) : true) 
    )
  }

  addEmployee() {
    this.router.navigate(['/dashboard/add']);
  }
  
  editEmployee(id: number) {
    this.router.navigate(['/dashboard/edit', id])
  }
  
  deleteEmployeeModal(modal: any, id: number) {
    this.selectedId = id
    this.modalService.open(modal, { centered: true })
  }

  deleteEmployee() {
    this.store.dispatch(deleteEmployee({ id: this.selectedId }))
  }

  detailEmployee(id: number) {
    let config = {
      filter: {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
      },
      sort: {
        active: this.dataSource.sort?.active,
        direction: this.dataSource.sort?.direction,
      },
      pagination: {
        index: this.dataSource.paginator?.pageIndex,
        size: this.dataSource.paginator?.pageSize,
      }
    }
    this.store.dispatch(saveConfig({config: config}))
    this.router.navigate(['/dashboard/detail/', id])
  }
}
