import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { inList } from 'src/app/_validators/inList.validator';
import { addEmployee } from 'src/app/_reducers/employee/employee.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  formGroup: any;
  employee: any;
  submitted: boolean;
  loading: boolean;
  today: Date;

  statusList = ['Employeed', 'Unemployeed'];
  groupList = ['Captivators', 'Hustlers', 'Mystics', 'Nomads', 'Noted', 'Royals', 'Seekers', 'Sluggers', 'Supremes', 'Underdogs'];

  constructor(private location: Location, private store: Store<{ employee: any }>, private formBuilder: FormBuilder) {
    this.today = new Date()
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      basicSalary: ['', [Validators.required, Validators.min(0), Validators.pattern(/^[0-9]\d*$/)]],
      status: ['', [Validators.required, inList(this.statusList)]],
      group: ['', [Validators.required, inList(this.groupList)]],
      birthDate: ['', [Validators.required]],
    })
  }

  isError(field: any) {
    return (this.submitted || this.formGroup.controls[field].touched || this.formGroup.controls[field].dirty) && this.formGroup.controls[field].errors !== null
  }

  goBack() {
    this.location.back();
  }

  save() {
    this.submitted = true
    if(this.formGroup.invalid) return
    this.loading = true
    let employee = {
      ...this.formGroup.value,
      birthDate: this.formGroup.value.birthDate.toISOString()
    }
    this.store.dispatch(addEmployee({employee: employee}))
  }
}
