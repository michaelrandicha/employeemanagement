import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { editEmployee } from 'src/app/_reducers/employee/employee.actions';
import { inList } from 'src/app/_validators/inList.validator';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  formGroup: any;
  employee: any;
  submitted: boolean;
  loading: boolean;
  today: Date;

  statusList = ['Employeed', 'Unemployeed'];
  groupList = ['Captivators', 'Hustlers', 'Mystics', 'Nomads', 'Noted', 'Royals', 'Seekers', 'Sluggers', 'Supremes', 'Underdogs'];

  constructor(private location: Location, private store: Store<{ employee: any }>, private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router) {
    this.today = new Date()
  }

  ngOnInit(): void {
    this.store.select('employee').subscribe(value => {
      this.employee = value.employee.find((employee: any) => employee.id == Number(this.route.snapshot.paramMap.get('id')))
      if (!this.employee) this.router.navigate(['/dashboard'])
      this.formGroup = this.formBuilder.group({
        id: [this.employee.id],
        firstName: [this.employee.firstName, [Validators.required]],
        lastName: [this.employee.lastName, [Validators.required]],
        username: [this.employee.username, [Validators.required]],
        email: [this.employee.email, [Validators.required, Validators.email]],
        basicSalary: [this.employee.basicSalary, [Validators.required, Validators.min(0), Validators.pattern(/^[0-9]\d*$/)]],
        status: [this.employee.status, [Validators.required, inList(this.statusList)]],
        group: [this.employee.group, [Validators.required, inList(this.groupList)]],
        birthDate: [new Date(this.employee.birthDate), [Validators.required]],
      })
    })
  }

  isError(field: any) {
    return (this.submitted || this.formGroup.controls[field].touched || this.formGroup.controls[field].dirty) && this.formGroup.controls[field].errors !== null
  }

  goBack() {
    this.location.back();
  }

  save() {
    this.submitted = true
    if(this.formGroup.invalid) return
    this.loading = true
    let employee = {
      ...this.formGroup.value,
      birthDate: this.formGroup.value.birthDate.toISOString()
    }
    this.store.dispatch(editEmployee({employee: employee}))
  }
}
