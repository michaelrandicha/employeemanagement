import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { login } from '../_reducers/auth/auth.actions';
import USER_DEFAULT from '../shared/default/user.default';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  formGroup: FormGroup
  loading: boolean
  submitted: boolean
  alert: any
  
  constructor(private formBuilder: FormBuilder, private store: Store<{ auth: boolean }>, private router: Router) {}
  
  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
    this.loading = false
    this.store.select('auth').subscribe(auth => {
      if (auth) this.router.navigate(['/dashboard'])
    })
  }
  
  isError(field: any) {
    return (this.submitted || this.formGroup.controls[field].touched || this.formGroup.controls[field].dirty) && this.formGroup.controls[field].errors !== null
  }
  
  onClosed() {
    this.alert = null
  }
  
  login() {
    this.submitted = true
    if(this.formGroup.invalid) return
    this.loading = true
    if(this.formGroup.value.username === USER_DEFAULT.username && this.formGroup.value.password === USER_DEFAULT.password) {
      this.store.dispatch(login())
    }else {
      this.loading = false
      this.alert = {
        type: 'danger',
        msg: `Incorrect username or password.`,
        timeout: 5000
      }
    }
  }
}
